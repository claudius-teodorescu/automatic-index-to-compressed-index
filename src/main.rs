use clap::error::ErrorKind;
use clap::{value_parser, Arg, Command};
use flate2::write::GzEncoder;
use flate2::Compression;
use fst::{MapBuilder, SetBuilder};
use glob::glob;
use sophia::api::prelude::*;
use sophia::inmem::graph::LightGraph;
use sophia::turtle::serializer::turtle::TurtleSerializer;
use sophia_api::term::SimpleTerm;
use std::collections::{BTreeMap, BTreeSet};
use std::fs::File;
use std::path::PathBuf;
use std::{fs, io, path};

const FIELD_DELIMITER: &str = "␟";
const FIELD_DELIMITER_2: &str = "-";
const METADATA_FILE_NAME: &str = "metadata.ttl";
const ONTOLOGY_BASE_IRI: &str = "https://kuberam.ro/ontologies/text-index#";
const METADATA_FOR_PROPERTY: &str = "metadataFor";
const IS_DEFAULT_INDEX_PROPERTY: &str = "isDefaultIndex";
const OUTPUT_TEMP_DIR_NAME: &str = "_index";

#[derive(Debug)]
struct IndexData {
    headings: BTreeMap<String, u64>,
    graph: LightGraph,
}

#[derive(Debug)]
struct IndexDescriptions {
    index_urls: Vec<String>,
    index_data: Vec<IndexData>,
    default_index_url: Option<String>,
}

impl IndexDescriptions {
    fn new() -> IndexDescriptions {
        let index_urls: Vec<String> = Vec::new();
        let index_data: Vec<IndexData> = Vec::new();
        let default_index_url = None;

        IndexDescriptions {
            index_urls,
            index_data,
            default_index_url,
        }
    }

    fn set_default_index_url(&mut self, default_index_url: String) {
        if self.default_index_url.is_some() {
            let current_default_index_url = self.default_index_url.clone().unwrap();
            panic!(
                "There are two indexes set as default ones: `{:?}` and `{:?}`!",
                default_index_url, current_default_index_url
            );
        } else {
            self.default_index_url = Some(default_index_url);
        }
    }
}

fn main() {
    // Get the arguments from the command line.
    let mut cmd = Command::new(env!("CARGO_PKG_NAME"))
        .author(env!("CARGO_PKG_AUTHORS"))
        .version(env!("CARGO_PKG_VERSION"))
        .about(env!("CARGO_PKG_DESCRIPTION"))
        .arg(
            Arg::new("index_as_triples_file_paths")
                .short('i')
                .value_parser(value_parser!(PathBuf))
                .num_args(1..),
        )
        .arg(
            Arg::new("automatic_index_file_path")
                .short('a')
                .value_parser(value_parser!(PathBuf)),
        );
    let cli_matches = cmd.clone().get_matches();
    let index_as_triples_file_paths: Vec<&PathBuf> = cli_matches
        .get_many("index_as_triples_file_paths")
        .expect("`index_as_triples_file_paths` is required")
        //.copied()
        .collect();
    let index_as_triples_file_paths_number = index_as_triples_file_paths.len();

    // Get the first index_as_triples_file_path, in order to make some validations.
    let index_as_triples_file_path = *index_as_triples_file_paths.get(0).unwrap();

    // Check if the first index_as_triples_file_path is a glob, and if they are more files.
    let first_path_entries: Vec<_> = glob(index_as_triples_file_path.to_str().unwrap())
        .map_err(|e| format!("Failed to read glob pattern: {}", e))
        .expect("")
        .collect();

    // If there are more than one first path indexes or index_as_triples_file_paths,
    // the argument automatic_index_file_path has to be set.
    if first_path_entries.len() == 1 && index_as_triples_file_paths_number == 1 {
        let output_dir = PathBuf::from(index_as_triples_file_path.parent().unwrap());
        run(index_as_triples_file_paths, &output_dir);
    } else {
        if !cli_matches.contains_id("automatic_index_file_path") {
            cmd.error(
                ErrorKind::MissingRequiredArgument,
                "The argument AUTOMATIC_INDEX_FILE_PATH or -a is required when there are multiple files corresponding the the argument INDEX_AS_TRIPLES_FILE_PATHS",
            )
            .exit();
        } else {
            let automatic_index_file_path = cli_matches
                .get_one("automatic_index_file_path")
                .expect("`automatic_index_file_path` is required");

            run(index_as_triples_file_paths, automatic_index_file_path);
        }
    }
}

fn run(index_as_triples_file_paths: Vec<&PathBuf>, automatic_index_file_path: &PathBuf) {
    // Initialize the global locators, meant to be used by all the indexes
    // that are compressed together.
    let mut locators: BTreeMap<String, u64> = BTreeMap::new();

    // Initialize the register with indexes' URLs
    let mut index_descriptions: IndexDescriptions = IndexDescriptions::new();

    // Initialize the graph for the metadata.
    let mut composite_metadata_graph: LightGraph = LightGraph::new();

    // Set the output dir.
    let output_dir = automatic_index_file_path.join(OUTPUT_TEMP_DIR_NAME.to_string());
    fs::create_dir_all(&output_dir).expect("Cannot create the `_index` subfolder");

    // This is the vector with indexes as graphs.

    // Preprocess the files with indexes as triples
    // (some of the paths could be globs). From this operation
    // will be obtained the global locators, the index register,
    // and an array with indexes as graphs.
    for index_as_triples_file_path in index_as_triples_file_paths.clone() {
        for entry in
            glob(index_as_triples_file_path.to_str().unwrap()).expect("Failed to read glob pattern")
        {
            match entry {
                Ok(path) => {
                    preprocess_index_as_triples(
                        path,
                        &mut index_descriptions,
                        &mut locators,
                        &mut composite_metadata_graph,
                    );
                }
                Err(e) => println!("{:?}", e),
            }
        }
    }

    // Index the locators.
    for (index, (_, value)) in locators.iter_mut().enumerate() {
        *value = index as u64 + 1;
    }

    // Process the data structures obtained after the previous operation.
    for (i, index_data) in index_descriptions.index_data.iter().enumerate() {
        process_index_as_triples(i, index_data, &mut locators, output_dir.clone());
    }

    // Serialize the locators to a file.
    create_index_map(output_dir.join("locators.fst"), locators.clone());

    // Serialize the composite metadata graph.
    let mut turtle_stringifier = TurtleSerializer::new_stringifier();
    let composite_metadata = turtle_stringifier
        .serialize_graph(&composite_metadata_graph)
        .expect("cannot serialize graph")
        .to_string();
    fs::write(output_dir.join("metadata.ttl"), &composite_metadata).expect("cannot write file.");

    // Archive all the files of the compressed index.
    let archive_file = File::create(automatic_index_file_path.join("index.tar.gz"))
        .expect("Cannot create the archive file");
    let encoding = GzEncoder::new(archive_file, Compression::new(9));
    let mut tar_builder = tar::Builder::new(encoding);
    tar_builder
        .append_dir_all(".", &output_dir)
        .expect("Cannot append to the archive file");

    // Remove the indexes' folder.
    fs::remove_dir_all(output_dir).expect("Cannot remove dir");
}

fn preprocess_index_as_triples(
    index_as_triples_file_path: PathBuf,
    index_descriptions: &mut IndexDescriptions,
    locators: &mut BTreeMap<String, u64>,
    composite_metadata_graph: &mut LightGraph,
) {
    // Some variables.
    let index_as_triples_parent_folder_path = index_as_triples_file_path.parent().unwrap();
    let mut index_url = "".to_string();

    // Retrieve the index metadata, which has to be in a file called metadata.ttl,
    // and register the index URl.
    let metadata_string =
        fs::read_to_string(index_as_triples_parent_folder_path.join(METADATA_FILE_NAME))
            .expect("Cannot load the metadata file. This file has to be named `metadata.ttl`.");
    let metadata = metadata_string.as_str();
    let metadata_graph: LightGraph = sophia::turtle::parser::turtle::parse_str(metadata)
        .collect_triples()
        .expect("cannot extract triples");

    // get the index URL
    let metadata_for_predicate = SimpleTerm::Iri(IriRef::new_unchecked(
        [ONTOLOGY_BASE_IRI, METADATA_FOR_PROPERTY].concat().into(),
    ));
    let metadata_for_triples =
        metadata_graph.triples_matching(Any, Some(metadata_for_predicate), Any);
    for triple in metadata_for_triples.into_iter() {
        let index_url_term = triple.unwrap()[2];

        if let Some(index_url_lexical_form) = index_url_term.iri() {
            let index_url_simple_term: &str = index_url_lexical_form.borrow_term();

            index_url = index_url_simple_term.to_string();
        }
    }

    // get the default index value
    let is_default_index_predicate = SimpleTerm::Iri(IriRef::new_unchecked(
        [ONTOLOGY_BASE_IRI, IS_DEFAULT_INDEX_PROPERTY]
            .concat()
            .into(),
    ));
    let is_default_index_triples =
        metadata_graph.triples_matching(Any, Some(is_default_index_predicate), Any);
    for triple in is_default_index_triples.into_iter() {
        let index_url_term = triple.unwrap()[2].lexical_form().unwrap();

        if index_url_term == "true" {
            index_descriptions.set_default_index_url(index_url.clone());
        }
    }

    // load the metadata graph into the composite metadata graph
    let _ = composite_metadata_graph
        .insert_all(metadata_graph.triples())
        .unwrap();

    // Load the index in a graph
    let index_as_triples_string = fs::read_to_string(&index_as_triples_file_path)
        .expect("Cannot load the metadata file. This file has to be named `index.ttl`.");
    let index_as_triples = index_as_triples_string.as_str();
    let index_as_triples_graph: LightGraph =
        sophia::turtle::parser::turtle::parse_str(index_as_triples)
            .collect_triples()
            .expect("cannot extract triples");

    // Process the index, in order to extract the locators and headings.
    // loop over the triples and extract the headings and locators
    let mut headings: BTreeMap<String, u64> = BTreeMap::new();
    for triple in index_as_triples_graph.triples().into_iter() {
        let triple = triple.unwrap();
        let heading_term = triple[2];
        let heading = get_term_value(heading_term);
        headings.insert(heading, 1u64);

        let locator_term = triple[0];
        let locator = get_term_value(locator_term);
        locators.insert(locator, 1u64);
    }

    // index the headings
    for (index, (_, value)) in headings.iter_mut().enumerate() {
        *value = index as u64 + 1;
    }

    // Add the index's data structures to the register.
    if Some(index_url.clone()) == index_descriptions.default_index_url {
        index_descriptions.index_urls.insert(0, index_url);
        index_descriptions.index_data.insert(
            0,
            IndexData {
                headings,
                graph: index_as_triples_graph,
            },
        );
    } else {
        index_descriptions.index_urls.push(index_url);
        index_descriptions.index_data.push(IndexData {
            headings,
            graph: index_as_triples_graph,
        });
    }
}

fn process_index_as_triples(
    i: usize,
    index_data: &IndexData,
    locators: &mut BTreeMap<String, u64>,
    output_dir: PathBuf,
) {
    // Some variables.
    let index_as_triples_graph = &index_data.graph;
    let headings = &index_data.headings;

    // Loop over the triples, to generate heading_ids_to_locators_ids.
    let mut heading_ids_to_locators_ids: BTreeMap<u64, BTreeSet<String>> = BTreeMap::new();
    let mut heading_ids_to_locators_ids_2: BTreeSet<String> = BTreeSet::new();
    for triple in index_as_triples_graph.triples().into_iter() {
        let triple = triple.unwrap();
        let heading_term = triple[2];
        let heading = get_term_value(heading_term);
        let heading_id = headings.get(&heading).unwrap();

        let locator_term = triple[0];
        let locator = get_term_value(locator_term);
        let locator_id = locators.get(&locator).unwrap();

        heading_ids_to_locators_ids
            .entry(*heading_id)
            .and_modify(|e| {
                e.insert(locator_id.to_string());
            })
            .or_insert({
                let mut set: BTreeSet<String> = BTreeSet::new();
                set.insert(locator_id.to_string());

                set
            });
        heading_ids_to_locators_ids_2
            .insert(format!("{}{}{}", heading_id, FIELD_DELIMITER_2, locator_id));
    }

    // Create the folders for the compressed index.
    let index_folder_path = output_dir.join(i.to_string());
    fs::create_dir_all(&index_folder_path).expect("Cannot create index subfolder");

    // Serialize to file the headings.
    create_index_map(index_folder_path.join("headings.fst"), headings.clone());

    // Serialize to file the heading_ids_to_locators_ids.
    let mut heading_ids_to_locators_ids_vector: Vec<String> = vec!["0".to_string()];
    for (_, value) in heading_ids_to_locators_ids.into_iter() {
        heading_ids_to_locators_ids_vector.push(Vec::from_iter(value).join(FIELD_DELIMITER));
    }
    /*fs::write(
        index_folder_path.join("heading_ids_to_locators_ids.json"),
        serde_json::to_string(&heading_ids_to_locators_ids_vector).expect("Cannot serialize"),
    )
    .expect("Cannot write file.");*/
    create_index_set(
        index_folder_path.join("heading_ids_to_locators_ids.fst"),
        heading_ids_to_locators_ids_2,
    );
}

fn get_term_value(term: &SimpleTerm) -> String {
    let term_kind = term.kind();

    let value = match term_kind {
        TermKind::BlankNode => panic!("Cannot process blank nodes!"),
        TermKind::Iri => term.iri().unwrap().as_str().to_string(),
        TermKind::Literal => term.lexical_form().unwrap().to_string(),
        TermKind::Triple => panic!("Cannot process triple nodes!"),
        TermKind::Variable => panic!("Cannot process variable nodes!"),
    };

    value
}

fn create_index_map(index_file_path: path::PathBuf, index_contents: BTreeMap<String, u64>) {
    let fst_file_handle =
        std::fs::File::create(index_file_path).expect("Cannot create the FST file.");
    let fst_buffered_writer = io::BufWriter::new(fst_file_handle);

    let mut fst_builder = MapBuilder::new(fst_buffered_writer).expect("Generate map builder.");

    for (key, val) in index_contents.iter() {
        fst_builder.insert(key, *val).unwrap();
    }

    fst_builder
        .finish()
        .expect("Finish generation of the FST map.");
}

fn create_index_set(index_file_path: path::PathBuf, index_contents: BTreeSet<String>) {
    let fst_file_handle =
        std::fs::File::create(index_file_path).expect("Cannot create the FST file.");
    let fst_buffered_writer = io::BufWriter::new(fst_file_handle);

    let mut fst_builder = SetBuilder::new(fst_buffered_writer).expect("Generate map builder.");

    for key in index_contents.iter() {
        fst_builder.insert(key).unwrap();
    }

    fst_builder
        .finish()
        .expect("Finish generation of the FST map.");
}

#[test]
fn test_cli() {
    let mut cmd = Command::new("prog")
        .arg(
            Arg::new("index_as_triples_file_paths")
                .num_args(1..)
                .short('i'),
        )
        .arg(
            Arg::new("automatic_index_file_path")
                //.num_args(0..1)
                .short('a'),
        );

    let matches_1 = cmd.clone().get_matches_from(vec![
        "prog",
        "-i",
        "/home/claudius/workspace/repositories/git/gitlab.com/claudius.teodorescu/dwds-data/public/peritext/indexes/lemmata-2/index.ttl",
        "/home/claudius/workspace/repositories/git/gitlab.com/claudius.teodorescu/dwds-data/public/peritext/indexes/**/index.ttl",
        "-a",
        "output-dir_1",
    ]);
    let index_as_triples_file_paths = matches_1
        .get_many::<String>("index_as_triples_file_paths")
        .expect("`index_as_triples_file_paths` is required");

    let index_as_triples_file_paths_number = index_as_triples_file_paths.len();

    // if there are more than one index_as_triples_file_paths,
    // the argument automatic_index_file_path has to be set
    if index_as_triples_file_paths_number > 1 {
        if !matches_1.contains_id("automatic_index_file_path") {
            cmd.error(
                ErrorKind::MissingRequiredArgument,
                "AUTOMATIC_INDEX_FILE_PATH or -a is required when there are multiple values for INDEX_AS_TRIPLES_FILE_PATHS",
            )
            .exit();
        }
    }

    for index_as_triples_file_path in index_as_triples_file_paths {
        for entry in glob(index_as_triples_file_path).expect("Failed to read glob pattern") {
            match entry {
                Ok(path) => println!("{:?}", path.display()),
                Err(e) => println!("{:?}", e),
            }
        }
    }
}

#[test]
fn test_1() {
    let mut index_as_triples_file_paths: Vec<&PathBuf> = Vec::new();
    let index_as_triples_file_path = PathBuf::from("/home/claudius/workspace/repositories/git/gitlab.rlp.net/adwmainz/nfdi4culture/cdmd/telemann-indexes/public/peritext/indexes/**/index.ttl");
    index_as_triples_file_paths.push(&index_as_triples_file_path);
    let output_dir = PathBuf::from("/home/claudius/workspace/repositories/git/gitlab.rlp.net/adwmainz/nfdi4culture/cdmd/telemann-indexes/public/peritext/indexes/_aggregated");

    run(index_as_triples_file_paths.clone(), &output_dir);
}

#[test]
fn test_2() {
    let mut index_as_triples_file_paths: Vec<&PathBuf> = Vec::new();
    let index_as_triples_file_path = PathBuf::from("/home/claudius/workspace/repositories/git/gitlab.com/claudius.teodorescu/dwds-data/public/peritext/indexes/**/index.ttl");
    index_as_triples_file_paths.push(&index_as_triples_file_path);
    let output_dir = PathBuf::from("/home/claudius/workspace/repositories/git/gitlab.com/claudius.teodorescu/dwds-data/public/peritext/indexes/_aggregated");

    run(index_as_triples_file_paths.clone(), &output_dir);
}

// cargo run -- -i "/home/claudius/workspace/repositories/git/gitlab.com/claudius.teodorescu/dwds-data/public/peritext/indexes/lemmata-2/index.ttl"
// cargo run -- -i "/home/claudius/workspace/repositories/git/gitlab.com/claudius.teodorescu/dwds-data/public/peritext/indexes/lemmata-2/index.ttl" "/home/claudius/workspace/repositories/git/gitlab.com/claudius.teodorescu/dwds-data/public/peritext/indexes/lemmata/index.ttl" -a "/home/claudius/workspace/repositories/git/gitlab.com/claudius.teodorescu/dwds-data/public/peritext/indexes/_aggregated"
// cargo run -- -i "/home/claudius/workspace/repositories/git/gitlab.com/claudius.teodorescu/dwds-data/public/peritext/indexes/**/index.ttl" -a "/home/claudius/workspace/repositories/git/gitlab.com/claudius.teodorescu/dwds-data/public/peritext/indexes/_aggregated"
