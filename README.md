# index-as-triples-to-compressed-index


## Scope

## To do list

* Fişa de cuvânt să fie TTL.

* Metadatele lucrărilor din corpus să fie separate, în TTL, şi să permită ordonări ale lucrărilor dintr-o fişa după autor, an etc. Al these metadata constitutes a bibliography.

* Change the index syntax to ```<urn:lexicon:ro:headword_with_diacritics_and_accents;lexicon_id> i:at <iri>```. The syntax for full text indexes could be ```<urn:lexicon:ro:headword;lexicon_id> i:at <iri> ; i:positions "positions"```. <#headword> sau urn ... forma grafică actuală? In this way, the IndexType class from ontology could be removed.

* Process the fragment-identifiers for the locators' IRIs; [https://datatracker.ietf.org/doc/html/rfc5147#page-6](https://datatracker.ietf.org/doc/html/rfc5147#page-6)

* Provide IRI validation (so that to allow only accepted fragment identifiers, namely ```char``` ones, but including id-s).

* Test with the data structure ```fst::Set``` for the fragment identifiers.

* Test union for ranges. This is needed for search by expressions with terms with wildcards.

* For Levenstein search, one approach could be to ship the respective automaton as a static &[u8] for distance 1, and 2, respectively.

* Add SimpleRegex automaton, maybe with the model found at https://github.com/BurntSushi/fst/pull/172.

* Make a new prototype, based upon INDOLOGY data.

* Make a new prototype, based upon DLR data. Test the search engine with full text indexes (unit test for Baza DLR) ("va să *ă").

* Data structure locator_id-heading_id-heading_position?

* For indexing: store word offsets in the published text segment, in order to not calculate them again in the browser, but to only highlight them?

* Is index-position_heading-id_locator-id a good structure? Test for smaller size.

Sizes of indexes in MB for DWDS (position were simulated with a simple counter):

| Index pos.  |  hl  |  lh  |
| :---------: | :-:  | :-:  |
| 0           | 2.3  | 2.3  |
| 1           | 0.74 | 1.1  |
| 2           | 0.37 | 1    |
| 3           | 0.18 | 0.3  |

| Index pos.  |  ihl |  ilh |
| :---------: | :-:  | :-:  |
| 0           | 2.3  | 2.3  |
| 1           | 0.74 | 1.1  |
| 2           | 0.37 | 1    |
| 3           | 0.18 | 0.3  |

| Index pos.  | ihlp | ilhp |
| :---------: | :-:  | :-:  |
| 0           | 3.6  | 3.7  |
| 1           | 2.5  | 2.7  |
| 2           | 2.3  | 2.5  |
| 3           | 2.2  | 2.7  |

## Index types, by search types:

* s_prefix_regex_levenstein (maps subject to triple id)

* s_ngram (maps subject to triple id)

* s_exact (maps subject to triple id)

* full text

## Data structures

The below data structures were tested, in order to see if they can provide access to a key by its value, needed for the locators of an index, and if they provide regex or Levenstein searches for keys, needed for the headings of an index.

* [Patricia Tree](https://docs.rs/patricia_tree) || No regex or Levenstein searches for keys, no access to keys by values. Serialization of the index with bincode leads to a file greater with 17% than the one from FST crate. It allows values as vectors.

* [trie-rs](https://github.com/laysakura/trie-rs) || No regex or Levenstein searches for keys, no access to keys by values. Serialization of the index with bincode leads to a file greater with 300% than the one from FST crate. It allows values as vectors.

### Data structure for headings

### Data structure for mapping locator identifiers to heading identifiers and for mapping position identifiers to locator identifiers

This structure maps provides one to many mapping, for each heading identifiers, with the corresponding locator identifiers.

The more adequate structure is ```fst::Set```, which stores the pairs ```heading_id{delimiter}locator_id```. Examples of such pairs: ```1-1, 1-2, 1-17```. This struucture is adequated both as to regards the size and speed of retriving data. For searching for expressions, when intersection of the pairs for each word is needed, the function ```intersection_by_suffix(suffix_delimiter)``` was added.

??This structure has to provide the following function: ```union``` (for when one has to find the locators for resources containing any of the respective headings).

The criteria for selecting the proper data structure:

* size of the compressed data structure;

* time for deserializing;

* time for getting the locator identifiers for one heading id;

* time for getting the locator identifiers obtained after an intersection for two headings id-s;

* time for union??.

#### Results for the testing with fst::Map

* Size of the compressed data structure: 33.8kB.

* Time for getting the locator identifiers for one heading id: range() = 18.61µs; starts_with() = 2.50µs; DFA = 578.15µs. The method used will be with starts_with().

* Time for getting the locator identifiers obtained after an intersection for two headings_id-doc_id sets: 40-70µs for +12K intersected with +24k pairs.

#### Results for the testing with roaring bitmaps

Note: This structure implies one bitmap for each word_id.

* Size of the compressed data structure: 70.2kB.

* Time for getting the locator identifiers for one heading id: .

* Time for getting the locator identifiers obtained after an intersection for two headings id-s: 215µs.

#### Results for the testing with vector of strings

* Time for getting the locator identifiers for one heading id: 500ns - 1.5µs.

* Time for getting the locator identifiers obtained after an intersection for two headings id-s:


#### Results for the testing with roaring bitmaps

* Size of the compressed data structure: 70.2kB.

* Time for getting the locator identifiers for one heading id: .

* Time for getting the locator identifiers obtained after an intersection for two headings id-s: 215µs.

### Data structure for locators

This structure stores the locators and provide access to them by their identifiers.

For this, a compact structure was chosen (```finite state transducer```), due to the fact that the locators are IRIs, which maybe have a prefix in commun, and this structure compresses them.

19.48µs

https://blog.burntsushi.net/transducers/


### Tests for searching in such compressed indexes
* Tested an index consisting of 332,135 triples (e.g., \<fraternal\> \<bibo:locator\> """{"51211":\[33\]}""" .), in size of 127MB (45MB compresssed), by using a search engine written in Rust and compiled to WebAssembly.

| Search type | Search string  | Result items no. | Time (ms) |
| ----------- | :-----------:  | :--------------: | --------- |
| regex       | ".*vanipura.*" | 2                | 43        |
| regex       | ".\*e0.\*"     | 376              | 48        |
| regex       | ".\*a3.\*"     | 1,525            | 5 4       |
| regex       | ".\*an.\*"     | 39,472           | 255       |
| regex       | ".\*a"         | 44,724           | 332       |
| regex       | ".\*a.\*"      | 181,697          | 1,393     |

".\*a.\*"
## Resources

* [Phrase Queries and Positional Indexes in C# - EximiaCo](https://eximia.co/phrase-queries-and-positional-indexes-in-c/)

* [Navarro: Compressed full-text indexes](https://scholar.google.com/scholar_lookup?title=Compressed+Full-text+Indexes&author=Navarro,+G.&author=M%C3%A4kinen,+V.&publication_year=2007&journal=ACM+Comput.+Surv.&volume=39&pages=2&doi=10.1145/1216370.1216372)

* [Algorithms | Free Full-Text | Towards Efficient Positional Inverted Index](https://mdpi.com/1999-4893/10/1/30)

* [Self-Indexing Natural Language](https://researchgate.net/publication/225175542_Self-Indexing_Natural_Language)

* [Building data structures that are smaller than an array and faster (in C)](https://news.ycombinator.com/item?id=3650657&ref=alexbowe.com)

* [Cap'n Proto: Introduction](https://capnproto.org/)

* [Rust to WebAssembly the hard way](https://surma.dev/things/rust-to-webassembly/)

* [Is possible in a single pass build a FULL JOIN for a in-memory structure (not using sql!)](https://stackoverflow.com/questions/52239451/is-possible-in-a-single-pass-build-a-full-join-for-a-in-memory-structure-not-us)

## Various resources
https://docs.rs/fst/latest/fst/raw/struct.Fst.html
https://github.com/wilsonzlin/edgesearch

<!--
DWDS, CITADA, Telemann, unit test
-->
